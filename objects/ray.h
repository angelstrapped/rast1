#ifndef RAY
#define RAY

#include "vector3.h"

typedef struct Ray {
    Vector3 origin;
    Vector3 direction;
} Ray;

#endif