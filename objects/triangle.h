#ifndef TRIANGLE
#define TRIANGLE

#include "vector3.h"

typedef struct Triangle {
    // Vertices.
    Vector3 v0;
    Vector3 v1;
    Vector3 v2;

    // Edges.
    Vector3 e1;
    Vector3 e2;
    
    // Normal.
    Vector3 n;

    // Color. Duh.
    Vector3 color;
} Triangle;

Triangle new_triangle(Vector3 v0, Vector3 v1, Vector3 v2, Vector3 color) {
    Vector3 e1 = { v1.x - v0.x, v1.y - v0.y, v1.z - v0.z };
    Vector3 e2 = { v2.x - v0.x, v2.y - v0.y, v2.z - v0.z };

    Vector3 n = cross(e1, e2);

    // This goes on the stack, and so goes out of scope as it gets returned?
    // Or is the entire struct passed as value?
    Triangle t = {v0, v1, v2, e1, e2, n, color};
    return t;
}

typedef struct Hit {
    float distance;
    Triangle mesh;
} Hit;

typedef struct Scene {
    // This naming convention is probably all wrong.
    size_t increment;
    size_t size;
    size_t element_size;
    size_t num_elements;
    Triangle *triangles;
} Scene;

#endif