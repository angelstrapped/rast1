#ifndef RASTERIZER
#define RASTERIZER

Pixel get_pixel(int canvas_width, int canvas_height);
void render_image(int canvas_width, int canvas_height, int num_channels, unsigned char *samples);

#endif
