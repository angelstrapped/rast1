LIBS= -lm -ljpeg
OBJECTS=rasterizer.o

all: clean build

clean:
	rm -f rast1 rasterizer.o

build: objects/vector3.h objects/camera.h objects/pixel.h rasterizer.o rasterizer.h
	gcc $(OBJECTS) rast1.c -o rast1 $(LIBS)

rasterizer.o: objects/vector3.h objects/camera.h objects/pixel.h rasterizer.c